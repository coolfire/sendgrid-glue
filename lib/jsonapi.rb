# frozen_string_literal: true

# Wrapper class to make http requests to json apis easier to work with
class JsonApi
  require 'httparty'
  require 'json'

  attr_accessor :headers

  def initialize(base, headers: nil, verify: true, ok_codes: nil)
    default_headers = {
      'Accept'       => 'application/json',
      'Content-Type' => 'application/json'
    }

    default_ok = [200, 201]

    @base     = base
    @headers  = headers.nil? ? default_headers : headers
    @verify   = verify
    @ok_codes = ok_codes.nil? ? default_ok : ok_codes
  end

  # Genering HTTP verb functions
  def get(path, query: nil)
    process_result(
      HTTParty.get(
        "#{@base}/#{path}",
        headers: @headers,
        verify: @verify
      )
    )
  end

  def post(path, body)
    process_result(
      HTTParty.post(
        "#{@base}/#{path}",
        body: body.to_json,
        headers: @headers,
        verify: @verify
      )
    )
  end

  def head(path); end

  def options(path); end

  def put(path, body); end

  def patch(path, body); end

  def delete(path); end

  private

  # Boilerplate function to check for errors and parse json response
  def process_result(res)
    raise "HTTP Error: #{res.code}: #{res.body}" unless @ok_codes.include? res.code

    JSON.parse res.body
  end
end
