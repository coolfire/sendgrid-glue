# frozen_string_literal: true

# Class to make http requests to sendgrid api
class Sendgrid
  require_relative 'jsonapi'

  def initialize(config)
    @config = config
    @http   = JsonApi.new(
      config.api.base
    )
    add_auth_header @http.headers
  end

  def testfn()
    @http.get(
      'stats'
    )
  end

  private

  def add_auth_header(headers)
    headers['Authorization'] = "Bearer #{@config.api.key}"
  end
end
