#!/usr/bin/env ruby
# frozen_string_literal: true

require_relative 'lib/config'
require_relative 'lib/sendgrid'
require 'logger'

config = load_config
logger = Logger.new config.logfile
sg     = Sendgrid.new config

puts sg.testfn
